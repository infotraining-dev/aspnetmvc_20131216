namespace PhoneBookMvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AttributesAndFluentChangesInSchema : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Phones", newName: "PhoneEntries");
            RenameColumn(table: "dbo.People", name: "LastName", newName: "Surname");
            RenameColumn(table: "dbo.PhoneEntries", name: "Number", newName: "Entry");
        }
        
        public override void Down()
        {
            RenameColumn(table: "dbo.PhoneEntries", name: "Entry", newName: "Number");
            RenameColumn(table: "dbo.People", name: "Surname", newName: "LastName");
            RenameTable(name: "dbo.PhoneEntries", newName: "Phones");
        }
    }
}
