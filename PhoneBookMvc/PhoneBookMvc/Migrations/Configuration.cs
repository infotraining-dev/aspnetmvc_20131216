namespace PhoneBookMvc.Migrations
{
    using PhoneBookMvc.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<PhoneBookMvc.DataContexts.PhoneBookDb>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(PhoneBookMvc.DataContexts.PhoneBookDb context)
        {
            context.People.AddOrUpdate(p => p.Id,
                new Person { Id = 1, FirstName = "Jan", LastName = "Kowalski", City = "Krak�w" },
                new Person { Id = 2, FirstName = "Adam", LastName = "Nowak", City = "Warszawa", IsFavourite = true },
                new Person { Id = 3, FirstName = "Anna", LastName = "Nowakowska", City = "Wroc�aw", IsFavourite = true });

            context.Phones.AddOrUpdate(p => p.Number,
                new Phone { Number = "12 456-44-33", PersonId = 1 },
                new Phone { Number = "22 543-55-44", PersonId = 2 },
                new Phone { Number = "71 666-33-34", PersonId = 3 });
        }
    }
}
