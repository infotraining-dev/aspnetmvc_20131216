namespace PhoneBookMvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsFavourite : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.People", "IsFavourite", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.People", "IsFavourite");
        }
    }
}
