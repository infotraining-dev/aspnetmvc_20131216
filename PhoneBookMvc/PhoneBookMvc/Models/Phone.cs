﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PhoneBookMvc.Models
{
    [Table("PhoneEntries")]
    public class Phone
    {
        public int Id { get; set; }

        [Required]
        [Display(Name="Numer telefonu")]
        [Column("Entry")]
        public string Number { get; set; }

        public virtual Person Person { get; set; }

        [Display(Name="ID właściciela telefonu")]
        [Required]
        public int PersonId { get; set; }
    }
}