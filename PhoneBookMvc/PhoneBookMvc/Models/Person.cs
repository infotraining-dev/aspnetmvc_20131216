﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PhoneBookMvc.Models
{
    public class Person
    {
        public int Id { get; set; }

        [Required(ErrorMessage="Imię jest wymagane")]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string City { get; set; }

        public bool IsFavourite { get; set; }

        public virtual ICollection<Phone> Phones { get; set; }

        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }

        public override string ToString()
        {
            return FullName + ", " + City;
        }
    }
}