﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PhoneBookMvc.DataContexts;
using PhoneBookMvc.Models;
using PhoneBookMvc.ViewModels;

namespace PhoneBookMvc.Controllers
{
    public class PhoneBookController : Controller
    {
        PhoneBookDb context = new PhoneBookDb();

        //
        // GET: /PhoneBook/

        public ActionResult Index()
        {
            var phones = context.Phones.OrderBy(p => p.Person.LastName).ToList();

            ViewBag.HeaderTitle = "Ksiażka telefoniczna";

            return View(phones);
        }

        public ActionResult Favourites()
        {
            var phones = context.Phones.Where(p => p.Person.IsFavourite).OrderBy(p => p.Person.LastName).ToList();

            ViewBag.HeaderTitle = "Telefony do osób na liście ulubionych";

            return View("Index", phones);
        }

        public ActionResult Insert()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Insert(Phone newPhone)
        {
            if (ModelState.IsValid)
            {
                context.Phones.Add(newPhone);
                context.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(newPhone);
        }

        public ActionResult Edit(int id)
        {
            var phone = context.Phones.Find(id);
            var people = context.People.ToList();

            PhoneViewModel phoneVM = new PhoneViewModel
            {
                Id = phone.Id,
                Number = phone.Number,
                People = new SelectList(people, "Id", "FullName"),
                PersonId = phone.PersonId
            };

            return View(phoneVM);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(PhoneViewModel phoneVM)
        {
            if (ModelState.IsValid)
            {
                Phone phone =
                    new Phone
                        {
                            Id = phoneVM.Id,
                            Number = phoneVM.Number,
                            PersonId = phoneVM.PersonId
                        };

                context.Phones.Attach(phone);
                context.Entry<Phone>(phone).State = System.Data.EntityState.Modified;
                context.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(phoneVM);
        }
    }
}
