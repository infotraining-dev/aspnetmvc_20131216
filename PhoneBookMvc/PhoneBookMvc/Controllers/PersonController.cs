﻿using PhoneBookMvc.DataContexts;
using PhoneBookMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoneBookMvc.Controllers
{
    public class PersonController : Controller
    {
        PhoneBookDb _context = new PhoneBookDb();

        //
        // GET: /Person/

        public ActionResult Index()
        {
            var people = _context.People.OrderBy(p => p.LastName).ToList();

            return View(people);
        }

        public ActionResult Edit(int id)
        {
            var person = _context.People.Find(id);

            return View(person);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Person person)
        {
            if (ModelState.IsValid)
            {
                _context.People.Attach(person);
                _context.Entry<Person>(person).State = System.Data.EntityState.Modified;

                _context.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(person);
        }

    }
}
