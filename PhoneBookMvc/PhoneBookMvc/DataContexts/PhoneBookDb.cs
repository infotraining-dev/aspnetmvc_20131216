﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PhoneBookMvc.Models;

namespace PhoneBookMvc.DataContexts
{
    public class PhoneBookDb : DbContext
    {
        public DbSet<Person> People { get; set; }
        public DbSet<Phone> Phones { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>()
                .Property(p => p.LastName)
                .HasColumnName("Surname")
                .HasMaxLength(255);

            base.OnModelCreating(modelBuilder);
        }
    }
}