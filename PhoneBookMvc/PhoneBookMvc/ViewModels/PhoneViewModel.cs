﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoneBookMvc.ViewModels
{
    public class PhoneViewModel
    {
        [Required]
        public int Id { get; set; }
        
        [Required]
        public string Number { get; set; }

        public IEnumerable<SelectListItem> People { get; set; }

        [Required]
        public int PersonId { get; set; }
    }
}