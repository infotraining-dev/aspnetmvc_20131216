﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomBinders.Models
{
    public class HomeModel
    {
        public string Title { get; set; }
        public string Date { get; set; }
    }
}