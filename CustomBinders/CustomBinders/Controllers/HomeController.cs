﻿using CustomBinders.Bindings;
using CustomBinders.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CustomBinders.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index([ModelBinder(typeof(CustomBinder))] HomeModel model)
        {
            if (ModelState.IsValid)
            {
                ViewBag.ModelTitle = model.Title;
                ViewBag.ModelDate = model.Date;
            }

            return View();
        }

        public ActionResult QueryStringOnly()
        {
            HomeModel model = new HomeModel();
   
            UpdateModel(model, new QueryStringValueProvider(ControllerContext));

            ViewBag.ModelTitle = model.Title;
            ViewBag.ModelDate = model.Date;

            return View("Index");
        }
    }
}
