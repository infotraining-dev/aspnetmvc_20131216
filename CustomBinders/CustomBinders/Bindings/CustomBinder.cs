﻿using CustomBinders.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CustomBinders.Bindings
{
    public class CustomBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            HttpRequestBase request = controllerContext.HttpContext.Request;

            string title = controllerContext.HttpContext.Request.Path;
            string day = request.Form["Day"];
            string month = request.Form["Month"];
            string year = request.Form["Year"];

            return new HomeModel { Title = title, Date = string.Format("{0}/{1}/{2}", day, month, year) };
        }
    }
}