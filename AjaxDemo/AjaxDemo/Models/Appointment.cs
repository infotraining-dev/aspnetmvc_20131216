﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AjaxDemo.Models
{
    public class Appointment
    {
        public string Client { get; set; }
        public DateTime Date { get; set; }
    }
}