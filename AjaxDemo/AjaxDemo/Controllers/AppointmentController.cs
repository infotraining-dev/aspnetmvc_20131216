﻿using AjaxDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace AjaxDemo.Controllers
{
    public class AppointmentController : Controller
    {
        //
        // GET: /Appointment/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string id)
        {
            return View("Index", (object)id);
        }


        public ActionResult AppointmentData(string id)
        {
            IEnumerable<Appointment> data = new[] {
                new Appointment{ Client = "Jan", Date = new DateTime(2013, 12, 20) },
                new Appointment{ Client = "Adam", Date = new DateTime(2013, 12, 21) },
                new Appointment{ Client = "Zenon", Date = new DateTime(2013, 12, 22) },
                new Appointment{ Client = "Zenon", Date = new DateTime(2013, 12, 23) },
                new Appointment{ Client = "Janina", Date = new DateTime(2013, 12, 24) },
                new Appointment{ Client = "Adam", Date = new DateTime(2013, 12, 24) },
            };

            if (!string.IsNullOrEmpty(id) && id != "Wszyscy")
                data = data.Where(a => a.Client == id);

            Thread.Sleep(5000);

            return View(data);
        }
    }
}
