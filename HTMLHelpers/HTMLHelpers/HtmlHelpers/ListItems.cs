﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTMLHelpers.HtmlHelpers
{
    public static class CustomHelpers
    {
        public static MvcHtmlString ListItemsExternal(this HtmlHelper html, IEnumerable<string> items)
        {
            TagBuilder tag = new TagBuilder("ul");

            foreach (var item in items)
            {
                TagBuilder itemTag = new TagBuilder("li");
                itemTag.SetInnerText(item);
                tag.InnerHtml += itemTag.ToString();
            }

            return new MvcHtmlString(tag.ToString());
        }
    }
}