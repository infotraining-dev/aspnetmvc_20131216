﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTMLHelpers.Controllers
{
    public class HelpersController : Controller
    {
        //
        // GET: /Helpers/

        public ActionResult Index()
        {
            List<string> cars = new List<string> { "Toyota", "Opel", "Mazda", "VW", "Polski Fiat" };

            List<string> cities = new List<string> { "Kraków", "Wrocław", "Warszawa", "Gdańsk" };

            ViewBag.Cars = cars;
            ViewBag.Cities = cities;

            return View();
        }

    }
}
