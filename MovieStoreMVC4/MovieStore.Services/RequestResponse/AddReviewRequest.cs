﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MovieStore.Services.ViewModels;

namespace MovieStore.Services.RequestResponse
{
    public class AddReviewRequest
    {
        public AddReviewViewModel Review { get; set; }
    }
}
