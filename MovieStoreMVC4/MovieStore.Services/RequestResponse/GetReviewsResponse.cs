﻿using MovieStore.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.Services.RequestResponse
{
    public class GetReviewsResponse : ResponseBase
    {
        public IEnumerable<ReviewViewModel> Reviews { get; set; }
    }
}
