﻿using AutoMapper;
using MovieStore.Model;
using MovieStore.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.Services.Mapping
{
    public static class BootstrapMapping
    {
        public static void Init()
        {
            Mapper.CreateMap<Review, ReviewViewModel>()
                .ForMember(rvm => rvm.ReviewDate, opt => opt.MapFrom(r => r.ReviewDate.ToString("d")));

            Mapper.CreateMap<AddReviewViewModel, Review>();

            Mapper.CreateMap<CartLine, CartLineViewModel>()
                .ForMember(t => t.MovieId, m => m.MapFrom(s => s.Movie.MovieID))
                .ForMember(t => t.MoviePrice, m => m.MapFrom(s => s.Movie.Price.ToString("c")))
                .ForMember(t => t.TotalPrice, m => m.ResolveUsing(s => string.Format("{0:c}", s.Quantity * s.Movie.Price)));

            Mapper.CreateMap<Cart, CartViewModel>()
                .ForMember(t => t.TotalQuantity, m => m.MapFrom(s => s.CartSummary.TotalQuantity))
                .ForMember(t => t.TotalValue, m => m.ResolveUsing(s => string.Format("{0:c}", s.CartSummary.TotalValue)));
        }
    }
}
