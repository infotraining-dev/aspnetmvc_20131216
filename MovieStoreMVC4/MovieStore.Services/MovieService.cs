﻿using MovieStore.Model;
using MovieStore.Model.Repositories;
using MovieStore.Services.Intefaces;
using MovieStore.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.Services
{
    public class MoviesService : IMoviesService
    {
        IMoviesRepository _moviesRepository;
        ICategoriesRepository _categoriesRepository;

        public MoviesService(IMoviesRepository moviesRepository, ICategoriesRepository categoriesRepository)
        {
            _moviesRepository = moviesRepository;
            _categoriesRepository = categoriesRepository;
        }

        public IEnumerable<Movie> GetMovies()
        {
            return _moviesRepository.All;
        }

        public IEnumerable<Category> GetCategories()
        {
            return _categoriesRepository.All;
        }

        public Movie GetMovieDetails(int id)
        {
            var movie = _moviesRepository.Find(id);

            return movie;
        }
    }
}
