﻿using AutoMapper;
using MovieStore.Model;
using MovieStore.Services.Interfaces;
using MovieStore.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.Services
{
    public class CartService : ICartService
    {
        public CartViewModel CreateCartViewModel(Cart cart)
        {
            return Mapper.Map<CartViewModel>(cart);
        }
    }
}
