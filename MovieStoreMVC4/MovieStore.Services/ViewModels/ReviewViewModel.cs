﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.Services.ViewModels
{
    public class ReviewViewModel
    {
        public int ReviewId { get; set; }

        [Required, Display(Name = "Autor")]
        public string Author { get; set; }

        public string ReviewDate { get; set; }

        [Required, DataType(DataType.MultilineText), Display(Name = "Recenzja")]
        public string Content { get; set; }

        [Display(Name = "Tytuł filmu")]
        public string MovieTitle { get; set; }

        public int MovieId { get; set; }

        [Required, Display(Name = "Ocena filmu")]
        public int Rating { get; set; }
    }
}
