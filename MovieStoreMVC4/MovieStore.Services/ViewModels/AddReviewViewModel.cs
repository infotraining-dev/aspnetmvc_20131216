﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.Services.ViewModels
{
    public class AddReviewViewModel : ReviewViewModel
    {
        private static SortedDictionary<int, string> _options
            = new SortedDictionary<int, string>()
                {
                    { 1, "Bardzo słaby"},
                    { 2, "Słaby" },
                    { 3, "Przeciętny" },
                    { 4, "Dobry" },
                    { 5, "Bardzo dobry" },
                    { 6, "Super" }
                };

        public AddReviewViewModel()
        {
            RatingList = _options;
        }

        public IDictionary<int, string> RatingList { get; private set; }  
    }
}
