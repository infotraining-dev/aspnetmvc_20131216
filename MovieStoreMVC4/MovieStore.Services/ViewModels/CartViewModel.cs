﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.Services.ViewModels
{
    public class CartLineViewModel
    {
        public int MovieId { get; set; }
        public string MovieTitle { get; set; }
        public string MoviePrice { get; set; }
        public string Quantity { get; set; }
        public string TotalPrice { get; set; }
    }

    public class CartViewModel
    {
        public IEnumerable<CartLineViewModel> Lines { get; set; }
        public int TotalQuantity { get; set; }
        public string TotalValue { get; set; }
    }
}
