﻿using MovieStore.Model;
using MovieStore.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.Services.Interfaces
{
    public interface ICartService
    {
        CartViewModel CreateCartViewModel(Cart cart);
    }
}
