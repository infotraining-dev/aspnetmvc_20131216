﻿using MovieStore.Model;
using MovieStore.Model.Repositories;
using MovieStore.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MovieStore.Services.Intefaces
{
    public interface IMoviesService
    {
        IEnumerable<Movie> GetMovies();
        IEnumerable<Category> GetCategories();
        Movie GetMovieDetails(int id);
    }

    
}
