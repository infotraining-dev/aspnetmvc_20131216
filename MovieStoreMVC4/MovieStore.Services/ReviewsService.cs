﻿using AutoMapper;
using MovieStore.Model;
using MovieStore.Model.Repositories;
using MovieStore.Services.Interfaces;
using MovieStore.Services.RequestResponse;
using MovieStore.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.Services
{
    interface IMapper
    {
        TDest Map<TSource, TDest>(TSource source);
    }

    public class ReviewsService : IReviewsService
    {
        IReviewsRepository _reviewsRepository;

        public ReviewsService(IReviewsRepository reviewsRepository)
        {
            _reviewsRepository = reviewsRepository;
        }

        public GetReviewsResponse GetReviews()
        {
            GetReviewsResponse response = new GetReviewsResponse();

            try
            {
                var reviews = _reviewsRepository.AllIncluding(r => r.Movie).OrderBy(r => r.ReviewDate);

                response.Reviews = Mapper.Map<IEnumerable<Review>, IEnumerable<ReviewViewModel>>(reviews);
                response.Success = true;
            }
            catch (Exception e)
            {
                response.Success = false;
                response.Message = e.Message;
                
            }

            return response;
        }

        public AddReviewViewModel CreateReviewViewModel(Movie movie)
        {
            return new AddReviewViewModel
            {
                MovieId = movie.MovieID,
                MovieTitle = movie.Title
            };
        }

        public AddReviewResponse AddReview(AddReviewRequest request)
        {
            AddReviewResponse response = new AddReviewResponse();

            try
            {
                Review review = Mapper.Map<Review>(request.Review);

                _reviewsRepository.InsertOrUpdate(review);
                _reviewsRepository.Save();

                response.Success = true;
            }
            catch (Exception e)
            {
                response.Success = false;
                response.Message = e.Message;
            }

            return response;
        }
    }
}
