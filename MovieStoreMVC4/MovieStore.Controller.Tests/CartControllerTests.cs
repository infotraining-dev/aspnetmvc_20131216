﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MovieStore.Model;
using MovieStore.Mvc4.Controllers;
using Moq;
using MovieStore.Model;
using System.Web.Mvc;
using System.Web;
using System.Web.Routing;
using System.Linq;
using MvcContrib.TestHelper;
using MovieStore.Model.Repositories;
using MovieStore.Services.Interfaces;
using MovieStore.Services.ViewModels;

namespace MovieStore.Mvc4.Controller.Tests
{
    [TestClass]
    public class CartControllerTests
    {
        Movie m1;
        Mock<IMoviesRepository> mqMoviesRepository;
        Mock<HttpContextBase> mqHttpContext;
        Mock<HttpSessionStateBase> mqSession;

        [TestInitialize]
        public void Setup()
        {
            m1 = new Movie { MovieID = 1, Title = "M1", Price = 100M };
            mqMoviesRepository = new Mock<IMoviesRepository>();
            mqMoviesRepository.Setup(m => m.Find(1)).Returns(m1);

            mqHttpContext = new Mock<HttpContextBase>();
            mqSession = new Mock<HttpSessionStateBase>();
        }

        [TestMethod]
        public void Can_Store_Cart_In_Session()
        {
            // Assign
            mqHttpContext.Setup(c => c.Session).Returns(mqSession.Object);

            CartController cartController = new CartController(mqMoviesRepository.Object, null);
            cartController.ControllerContext = new ControllerContext(mqHttpContext.Object, new RouteData(), cartController);

            // Act
            cartController.AddToCart(1);

            // Assert
            mqSession.VerifySet(s => s["Cart"] = It.IsAny<Cart>());
        }

        [TestMethod]
        public void Can_Add_Movie_To_Cart()
        {
            Cart cart = new Cart();
            mqSession.Setup(s => s["Cart"]).Returns(cart);
            mqHttpContext.Setup(c => c.Session).Returns(mqSession.Object);

            CartController cartController = new CartController(mqMoviesRepository.Object, null);
            cartController.ControllerContext = new ControllerContext(mqHttpContext.Object, new RouteData(), cartController);

            cartController.AddToCart(1);

            var lines = cart.Lines.ToList();
            Assert.AreEqual(lines.Count(), 1);
            Assert.AreEqual(lines[0].Movie.MovieID, 1);
            Assert.AreEqual(lines[0].Quantity, 1);
        }

        [TestMethod]
        public void Can_Remove_Line()
        {
            Cart cart = new Cart();
            cart.AddItem(m1);

            TestControllerBuilder controllerBuilder = new TestControllerBuilder();
            controllerBuilder.Session["Cart"] = cart;

            Mock<ICartService> mqCartService = new Moq.Mock<ICartService>();
            mqCartService.Setup(m => m.CreateCartViewModel(It.IsAny<Cart>())).Returns(new CartViewModel());
            
            CartController cartController = new CartController(mqMoviesRepository.Object, mqCartService.Object);
            controllerBuilder.InitializeController(cartController);

            cartController.RemoveLine(1);

            Assert.AreEqual(0, cart.Lines.Count());
        }
    }
}
