﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using MovieStore.Model;
using System.Collections.Generic;
using System.Collections;

namespace MovieStore.Controller.Tests
{
    [TestClass]
    public class WhenCallingCategoryList : WithMoviesController
    {
        PartialViewResult _result;
        IEnumerable<Category> _expected = new List<Category>
        {
            new Category { CategoryID = 1, Name = "C1" },
            new Category { CategoryID = 2, Name = "C2" },
            new Category { CategoryID = 3, Name = "C3" },
        };

        protected override void EstablishContext()
        {
            base.EstablishContext();

            _moviesService.Setup(m => m.GetCategories()).Returns(_expected);
        }

        protected override void BecauseOf()
        {
            _result = _sut.CategoryList() as PartialViewResult;
        }

        [TestMethod]
        public void ShouldCallGetCategoriesOnService()
        {
            _moviesService.Verify(m => m.GetCategories());
        }

        [TestMethod]
        public void ShouldReturnCategoriesToView()
        {
            var categories = _result.Model as ICollection;

            CollectionAssert.AreEqual(_expected as ICollection, categories);
        }
    }
}
