﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MovieStore.Mvc4.Controllers;
using System.Web.Mvc;
using Moq;
using MovieStore.Services;
using MovieStore.Model;
using System.Collections.Generic;
using System.Collections;
using MovieStore.BDDFramework;
using MovieStore.Services.Intefaces;

namespace MovieStore.Controller.Tests
{
    public class WithMoviesController : Context
    {
        protected MoviesController _sut;
        protected Mock<IMoviesService> _moviesService;

        protected override void EstablishContext()
        {
            _moviesService = new Mock<IMoviesService>();
            _sut = new MoviesController(_moviesService.Object);
            
        }
    }

    [TestClass]
    public class WhenCallingIndex : WithMoviesController
    {
        ActionResult _result;

        ICollection _expected = new List<Movie>()
            {
                new Movie { MovieID = 1, Title = "M1" },
                new Movie { MovieID = 2, Title = "M2" },
                new Movie { MovieID = 3, Title = "M3" },
            };

        protected override void EstablishContext()
        {
            base.EstablishContext();

            _moviesService.Setup(m => m.GetMovies()).Returns(_expected as IEnumerable<Movie>);
        }


        protected override void BecauseOf()
        {
            _result = _sut.Index();
        }

        [TestMethod]
        public void ShouldCallGetMoviesOnService()
        {
            _moviesService.Verify(m => m.GetMovies());
        }

        [TestMethod]
        public void ShouldReturnMoviesToView()
        {
            ViewResult viewResult = (ViewResult)_result;

            Assert.IsInstanceOfType(viewResult.Model, typeof(IEnumerable<Movie>));
            CollectionAssert.AreEqual(_expected, (ICollection)viewResult.Model);
        }


    }
}
