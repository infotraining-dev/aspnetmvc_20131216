﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MovieStore.BDDFramework;
using MovieStore.Mvc4.Controllers;
using Ninject;
using System.Web.Mvc;
using System.Collections.Generic;
using MovieStore.Model;
using System.Collections;

namespace MovieStore.AcceptanceTests
{
    [TestClass]
    public class DisplayingCategoryList : GWT
    {
        MoviesController _sut;
        ActionResult _result;

        protected override void Given()
        {
            StandardKernel kernel = new StandardKernel(new MovieStoreModule());
            _sut = kernel.Get<MoviesController>();
        }

        protected override void When()
        {
            _result = _sut.CategoryList();
        }

        [TestMethod]
        [TestCategory("Acceptance")]
        public void ShouldReturnAPartialView()
        {
            Assert.IsInstanceOfType(_result, typeof(PartialViewResult));
        }

        [TestMethod]
        [TestCategory("Acceptance")]
        public void ShouldReturnListOfCategories()
        {
            ICollection expected = CreateCategoryList();

            var model = (_result as PartialViewResult).Model;
            CollectionAssert.AreEqual(expected, (ICollection)model, new CategoryComparer());
        }

        private ICollection CreateCategoryList()
        {
            return new List<Category> 
            {
                new Category { CategoryID = 1, Name = "Dramat" },
                new Category { CategoryID = 2, Name = "Komedia" },
                new Category { CategoryID = 3, Name = "Thriller" },
                new Category { CategoryID = 4, Name = "Horror" },
                new Category { CategoryID = 5, Name = "Sensacyjny" },
                new Category { CategoryID = 6, Name = "Serial" },
                new Category { CategoryID = 7, Name = "Biograficzny" }
            };
        }
    }
}
