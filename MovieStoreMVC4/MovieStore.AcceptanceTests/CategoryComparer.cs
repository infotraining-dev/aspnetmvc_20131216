﻿using MovieStore.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MovieStore.AcceptanceTests
{
    public class CategoryComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            Category cx = (Category)x;
            Category cy = (Category)y;

            return cx.CategoryID.CompareTo(cy.CategoryID);
        }
    }
}
