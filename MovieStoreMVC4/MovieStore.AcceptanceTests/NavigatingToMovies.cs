﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MovieStore.Mvc4.Controllers;
using System.Web.Mvc;
using MovieStore.Model;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using MovieStore.Services;
using MovieStore.BDDFramework;
using MovieStore.DataAccessLayer.Database.Repositories;
using Ninject.Modules;
using MovieStore.Model.Repositories;
using Ninject;
using MovieStore.Services.Intefaces;

namespace MovieStore.AcceptanceTests
{
    class MovieComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            Movie mx = (Movie)x;
            Movie my = (Movie)y;

            return mx.MovieID.CompareTo(my.MovieID);
        }
    }

    public class MovieStoreModule : NinjectModule
    {
        public override void Load()
        {
            this.Bind<IMoviesService>().To<MoviesService>();
            this.Bind<IMoviesRepository>().To<MoviesRepository>();
            this.Bind<ICategoriesRepository>().To<CategoriesRepository>();
        }
    }


    [TestClass]
    public class NavigatingToMovies : GWT
    {
        private MoviesController _sut;
        private ActionResult _result;

        protected override void Given()
        {
            StandardKernel kernel = new StandardKernel(new MovieStoreModule());

            _sut = kernel.Get<MoviesController>();
        }

        protected override void When()
        {
            _result = _sut.Index();
        }

        [TestMethod]
        [TestCategory("Acceptance")]
        public void ShouldReturnAView()
        {
            Assert.IsInstanceOfType(_result, typeof(ViewResult));
        }

        [TestMethod]
        [TestCategory("Acceptance")]
        public void ShouldReturnListOfMovies()
        {
            ViewResult vr = (ViewResult)_result;

            ICollection movies = (ICollection)vr.Model;
            var expected = CreateMovieList();

            Assert.IsNotNull(movies);

            CollectionAssert.AreEqual(expected, movies, new MovieComparer() );
        }

        private ICollection CreateMovieList()
        {
 	        return new List<Movie>()
            {
                new Movie { MovieID = 1, CategoryID = 1, Title = "Dom zły", Price = 49.99M, Cover = "dom-zly.png", CoverThumbnail = "dom-zly-thumbnail.png", Description = "Pewnej deszczowej nocy Edward Środoń (Arkadiusz Jakubik) pojawia się przypadkowo w domu małżeństwa Dziabasów (Kinga Preis i Marian Dziędziel). Początkowa nieufność gospodarzy ustępuje miejsca tradycyjnej polskiej gościnności. Przybysz nie przypuszcza nawet, jak bardzo to spotkanie odmieni jego życie. Po kilku latach, w tym samym domu ekipa śledcza rozpoczyna dochodzenie. W progu ponownie staje Edward Środoń. Tym razem jego wizyta nie jest przypadkowa – ma pomóc w rekonstrukcji tajemniczych zdarzeń sprzed czterech lat. Prowadzący śledztwo porucznik Mróz (Bartłomiej Topa) odkrywa, że rozwiązanie zagadki może być niebezpieczne nie tylko dla podejrzanego. Rozpoczyna się dramatyczny wyścig z czasem.", ShortDescription = "Jest rok 1978. Do domu Zdzisława i Bożeny Dziobasów trafia przypadkowy gość, Edward Środoń. Po kolacji zakrapianej alkoholem dochodzi do tragedii." },
                new Movie { MovieID = 2, CategoryID = 3, Title = "Prestiż", Price = 39.99M, Cover = "prestiz.png", CoverThumbnail = "prestiz-thumbnail.png", Description = "Rywalizacja dwóch iluzjonistów, o to który z nich jest w stanie stworzyć wspanialszą iluzję", ShortDescription = "Rywalizacja dwóch iluzjonistów, o to który z nich jest w stanie stworzyć wspanialszą iluzję." },
                new Movie { MovieID = 3, CategoryID = 6, Title = "Dexter", Price = 149.99M, Cover = "dexter.png", CoverThumbnail = "dexter-thumbnail.png", Description = "Dexter jest seryjnym mordercą. W dzieciństwie został przygarnięty przez policjanta, Harry'ego, który nauczył go zaspokajać swoje chore pragnienia, będące wynikiem traumatycznych przeżyć. Dexter, posługując się \"kodeksem\" przybranego ojca, zabija ludzi, którzy sobie (w jego mniemaniu) na to zasłużyli, a jednocześnie swojemu \"głodowi\" zabijania mówi stanowcze nie!", ShortDescription = "Dexter prowadzi podwójne życie. Za dnia jest cenionym specjalistą ds. krwi w departamencie policji, a nocą zabija złoczyńców, którzy wymykają się organom sprawiedliwości." },
                new Movie { MovieID = 4, CategoryID = 1, Title = "Amerykanin", Price = 69.00M, Cover = "Amerykanin.png", CoverThumbnail = "Amerykanin-thumbnail.png", Description = "Jack (George Clooney) jest płatnym zabójcą, najlepszym w swoim fachu. Gdy podczas jednego ze zleceń ginie jego kochanka, postanawia definitywnie wycofać się z zawodu. Ostatnia misja Jacka rozegra się w malowniczym włoskim miasteczku. Ostrożny w kontaktach z ludźmi, będąc pod wpływem urokliwego miejsca, w którym przyszło mu wykonać niebezpieczne zadanie, z czasem zaprzyjaźnia się z Ojcem Benedetto (Paolo Bonacelli), jak również wdaje się w płomienny romans z Clarą (Violante Placido). Wszystko to mocno skomplikuje jego pracę.", ShortDescription = "Włoskie plenery, wciągająca historia i Clooney w najwyższej życiowej formie." },
                new Movie { MovieID = 5, CategoryID = 4, Title = "Zejście", Price = 80.99M, Cover = "Zejscie.png", CoverThumbnail = "Zejscie-thumbnail.png", Description = "Film opowiada historię sześciu przyjaciółek, które postanawiają spędzić wspólnie kilka dni na eksploracji górskich jaskiń. Niestety, nagły wypadek zmienia ich sympatyczne wakacje w koszmar. Uwięzione w nieznanej grocie, bez jedzenia i mapy muszą odnaleźć drogę do wyjścia. Nie wiedzą jednak, że jaskinia kryje w sobie przerażającą tajemnicę.", ShortDescription = "Brutalny horror, z dużą ilością krwi, otwartych ran i popisów gimnastycznych." },
                new Movie { MovieID = 6, CategoryID = 2, Title = "Heartbreaker. Licencja na uwodzenie", Price = 55.00M, Cover = "Heartbreaker.png", CoverThumbnail = "Heartbreaker-thumbnail.png", Description = "Przystojny uwodziciel świadczy płatne usługi polegające na rozbijaniu nieudanych par. Na zlecenie pomaga w ten sposób wszystkim rodzajom kobiet: matkom, siostrom, córkom, przyjaciółkom, koleżankom z pracy. Jego cel to otwarcie ich oczu. Jego metoda to uwodzenie.", ShortDescription = "Urocza, francuska komedia romatyczna." },
                new Movie { MovieID = 7, CategoryID = 3, Title = "Incepcja", Price = 65.00M, Cover = "Incepcja.png", CoverThumbnail = "Incepcja-thumbnail.png", Description = "Film opowiadający o możliwościach ingerowania w ludzki umysł dzięki zaawansowanej technologii. Głównym bohaterem filmu jest Cobb, który jest szefem zespołu specjalizującego się w dokonywaniu podróży do umysłów innych osób. Dzięki tej możliwości może zarówno pozyskiwać informacje, jak i wprowadzać nowe dane.", ShortDescription = "Film zabiera widzów wgłąb nieskończonego świata snów." },
                new Movie { MovieID = 8, CategoryID = 2, Title = "Wszystko w porządku", Price = 58.00M, Cover = "Thekids.png", CoverThumbnail = "Thekids-thumbnail.png", Description = "Film śledzi losy Nic i Jules, pary w średnim wieku, która na przedmieściach Los Angeles wychowuje dwójkę nastoletnich dzieci - Joni i Lasera. Wszystko wydaje się być w porządku do momentu, w którym Joni kończy 18 lat i zostaje namówiona przez brata do odszukania ich biologicznego ojca. Z wahaniem wykonuje telefon do banku spermy, który łączy ją z Paulem, właścicielem restauracji. Paul okazuje się być chętny do poznania swoich dzieci, o których nigdy nie miał pojęcia. Od kiedy pojawia się on w życiu rodziny, zaczynają się problemy narażające stabilność tej pozornie normalnej rodziny.", ShortDescription = "Komedia, która wywraca do góry nogami wszystko, co wiesz o współczesnej rodzinie." },
                new Movie { MovieID = 9, CategoryID = 1, Title = "Lektor", Price = 49.00M, Cover = "Lektor.png", CoverThumbnail = "Lektor-thumbnail.png", Description = "Oparty na bestsellerowej powieści o tym samym tytule „Lektor”, to hipnotyzująca historia pełnej tajemnic miłości, której akcja toczy się w targanych wojną i powojennych Niemczech. Hanna (Kate Winslet) udziela pomocy Michaelowi Bergowi (David Kross), który zasłabł na ulicy, wracając ze szkoły. W ten sposób krzyżują się losy młodego wrażliwego ucznia i pięknej, choć starszej od niego kobiety, którą jest zafascynowany od pierwszej chwili. To przypadkowe spotkanie staje się początkiem namiętnego romansu, zakończonego nagłym i tajemniczym zniknięciem kobiety. Mija osiem lat. Michael jest studentem prawa i śledzi procesy nazistowskich zbrodniarzy. W tych właśnie, jakże odmiennych, okolicznościach spotyka ponownie swoją dawną kochankę. Hanna oskarżona jest o popełnienie odrażających zbrodni.", ShortDescription = "Hipnotyzująca historia pełnej tajemnic miłości, której akcja toczy się w targanych wojną i powojennych Niemczech." },
                new Movie { MovieID = 10, CategoryID = 1, Title = "Droga do szczęścia", Price = 52.00M, Cover = "Droga.png", CoverThumbnail = "Droga-thumbnail.png", Description = "Film zrealizowany na podstawie książki Richarda Yatesa. April i Frank Wheeler to młode małżeństwo mieszkające z dwójką dzieci na przedmieściach Connecticut w połowie lat 50-tych. Oboje skutecznie maskują frustrację spowodowaną niemożnością spełnienia się zarówno w związku jak i karierze zawodowej. Frank ugrzązł w dobrze płatnej, ale nudnej pracy biurowej. Gospodyni domowa, April, ciągle opłakuje stojącą kiedyś przed nią szansę na zrobienie kariery aktorskiej. Nie mogąc znieść przeciętności, jaka ich otacza, decydują się wyjechać do Francji, gdzie będą w stanie rozwijać swoją wrażliwość artystyczną wolni od konsumpcyjnych wymagań kapitalistycznej Ameryki. Kiedy jednak ich związek zamienia się w nieskończone pasmo sprzeczek, zazdrości i wzajemnych oskarżeń, ich podróż oraz marzenia o samospełnieniu stają w obliczu zagrożenia.", ShortDescription = "Sam Mendes powraca na amerykańskie przedmieścia, by raz jeszcze pokazać dojmujący smutek pozornie udanej egzystencji." },
                new Movie { MovieID = 11, CategoryID = 1, Title = "Persepolis", Price = 29.00M, Cover = "Persepolis.png", CoverThumbnail = "Persepolis-thumbnail.png", Description = "Komiks przedstawia niezapomniany obraz codziennego życia w Iranie, pełnego jaskrawych i często zaskakujących sprzeczności pomiędzy życiem rodzinnym a sferą publiczną. Dziecięce spojrzenie Marjane na zdetronizowanych władców, na państwowo usankcjonowane i metodycznie przeprowadzane kary, oraz na bohaterów rewolucji pozwala poznać historię tego fascynującego kraju i jej niezwykłej rodziny. \"Persepolis\" jest utworem oryginalnym, niezwykle osobistym i wnikliwym politycznie. To z jednej strony opowieść o dorastaniu a z drugiej zaś przypomnienie kosztów, jakie ponoszą ludzie żyjący w czasie wojny w kraju politycznych represji. Pokazuje jak można radzić sobie ze śmiechem i łzami w obliczu tak absurdalnej rzeczywistości oraz - wreszcie, przedstawia nam zniewalającą małą bohaterkę, której po prostu nie można się oprzeć.", ShortDescription = "W wyrazistych, czarno-białych rysunkach Satrapi zawarła historię swojego codziennego życia w Teheranie." },
                new Movie { MovieID = 12, CategoryID = 1, Title = "Samotny mężczyzna", Price = 69.00M, Cover = "Samotny.png", CoverThumbnail = "Samotny-thumbnail.png", Description = "Jeden dzień – być może najważniejszy – z życia Georga Falconera (Colin Firth), profesora uniwersyteckiego, który nie może pogodzić się z tym, że właśnie stracił swoją wielką miłość. Tego dnia pozornie nieistotne spotkania i czas spędzony z dawną ukochaną (Julianne Moore), otworzą przed nim niespodziewanie szanse na nowe uczucie.", ShortDescription = "Porażający zmysłowym pięknem debiut Toma Forda, słynnego kreatora mody." },
                new Movie { MovieID = 13, CategoryID = 7, Title = "The Social Network", Price = 69.00M, Cover = "Social.png", CoverThumbnail = "Social-thumbnail.png", Description = "Pewnego październikowego wieczoru w 2003 roku, po zerwaniu z dziewczyną, Mark (Jesse Eisenberg) włamuje się do uniwersyteckiej sieci komputerowej i tworzy stronę internetową będącą bazą studentek Harvardu. Następnie umieszcza obok siebie zdjęcia. Strona otrzymuje nazwę Facemash i staje się niezwykle popularna wywołując jednocześnie mnóstwo kontrowersji. Mark zostaje oskarżony o celowe złamanie zabezpieczeń, pogwałcenie praw autorskich i naruszenie prywatności. Jednak to właśnie wtedy rodzi się zarys Facebooka. Wkrótce potem Mark zakłada stronę thefacebook.com, która gości na monitorach komputerów na Harvardzie, uniwersytetów Ivy League oraz Doliny Krzemowej, a potem dosłownie w każdym zakątku świata.", ShortDescription = "Film opowiada o powstaniu portalu Facebook." },
                new Movie { MovieID = 14, CategoryID = 6, Title = "Sześć stóp pod ziemią", Price = 550.00M, Cover = "Sześć stóp pod ziemią.png", CoverThumbnail = "Sześć stóp pod ziemią-thumbnail.png", Description = "Dom pogrzebowy Fisher & Diaz to miejsce wielu nietypowych wydarzeń, a jego pracownicy Nate (Peter Krause), Ruth (Frances Conroy), David (Michael C. Hall) i Claire (Lauren Ambrose) to świadkowie niezwykłych ludzkich zachowań. Niekonwencjonalna śmierć i dziwaczne zbiegi okoliczności dawno temu przestały robić na nich wrażenie. Co innego życie prywatne - tam, każde z nich próbuje odnaleźć sens...", ShortDescription = "Przesycony czarnym humorem serial obyczajowy o nietypowej rodzinie z Los Angeles, która prowadzi dom pogrzebowy." },
                new Movie { MovieID = 15, CategoryID = 6, Title = "Współczesna rodzina", Price = 450.00M, Cover = "Współczesna rodzina.png", CoverThumbnail = "Współczesna rodzina-thumbnail.png", Description = "Serial komediowy w konwencji mockumentary (pseudodokumentu), opowiadający o losach wielopokoleniowej i wielokulturowej amerykańskiej rodziny. Nestorem rodu jest Jay Pritchett (Ed O’Neill, znany głównie z roli Ala Bundy’ego), który po rozwodzie żeni się z o wiele młodszą od siebie Kolumbijką Glorią, adoptując przy okazji jej syna z pierwszego małżeństwa, dziesięcioletniego Manny’ego. Dziećmi Jaya są Claire Dunphy, mieszająca z mężem Philem, córkami Haley i Alex i synem Lukiem oraz Mitchell, który wraz ze swoim chłopakiem - Cameronem - wychowuje adoptowaną w Wietnamie córkę.", ShortDescription = "Serial komediowy w konwencji pseudodokumentu, opowiadający o losach wielopokoleniowej i wielokulturowej amerykańskiej rodziny." }
            };
        }
    }
}
