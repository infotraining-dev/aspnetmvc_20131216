﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieStore.Model
{
    public class Category
    {
        [Key]
        [Display(Name = "ID Kategorii")]
        public int CategoryID { get; set; }

        [Required, Display(Name = "Nazwa")]
        public string Name { get; set; }

        public virtual ICollection<Movie> Movies { get; set; }
    }
}
