﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.Model
{
    public class CartLine
    {
        public Movie Movie { get; set; }
        public int Quantity { get; set; }
    }

    public class CartSummary
    {
        public int TotalQuantity { get; set; }
        public decimal TotalValue { get; set; }
    }

    public class Cart
    {
        private List<CartLine> _lines = new List<CartLine>();

        public decimal TotalValue
        {
            get { return _lines.Sum(cl => cl.Movie.Price * cl.Quantity); }
        }

        public IEnumerable<CartLine> Lines
        {
            get { return _lines; }
        }

        public void AddItem(Movie movie, int quantity = 1)
        {
            CartLine line = _lines.FirstOrDefault(cl => cl.Movie.MovieID == movie.MovieID);

            if (line == null)
            {
                _lines.Add(new CartLine { Movie = movie, Quantity = quantity });
            }
            else
            {
                line.Quantity += quantity;
            }
        }

        public void RemoveLine(Movie movie)
        {
            _lines.RemoveAll(cl => cl.Movie.MovieID == movie.MovieID);
        }

        public void Clear()
        {
            _lines.Clear();
        }

        public CartSummary CartSummary
        {
            get 
            { 
                return new CartSummary 
                    { 
                        TotalQuantity = _lines.Sum(cl => cl.Quantity), 
                        TotalValue = this.TotalValue 
                    }; 
            }
        }
    }
}
