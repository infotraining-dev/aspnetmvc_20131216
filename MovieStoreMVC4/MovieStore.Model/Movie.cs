﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieStore.Model
{
    public class Movie
    {
        [Key]
        [Display(Name = "ID Filmu")]
        public int MovieID { get; set; }

        [Required]
        [Display(Name = "Tytuł")]
        public string Title { get; set; }

        [Display(Name = "Kategoria")]
        public int CategoryID { get; set; }

        [Display(Name = "Kategoria")]
        public virtual Category Category { get; set; }

        [Required]
        [Display(Name = "Opis krótki"), DataType(DataType.MultilineText)]
        public string ShortDescription { get; set; }

        [Required]
        [Display(Name = "Opis długi"), DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Okładka")]
        public string Cover { get; set; }

        [Required]
        [Display(Name = "Okładka - miniatura")]
        public string CoverThumbnail { get; set; }

        [Required]
        [Display(Name = "Cena")]
        [DisplayFormat(DataFormatString = "{0:c}"), DataType(DataType.Currency)]
        public decimal Price { get; set; }

        public virtual ICollection<Review> Reviews { get; set; }
    }
}