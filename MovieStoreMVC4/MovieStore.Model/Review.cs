﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MovieStore.Model
{
    public class Review
    {
        public Review()
        {
            ReviewDate = DateTime.Now;
        }

        [Key]
        [Display(Name = "ID Oceny")]
        public int ReviewID { get; set; }

        public int MovieID { get; set; }

        [Display(Name = "Film")]
        public virtual Movie Movie { get; set; }

        [Required]
        [Display(Name = "Nazwa recenzenta")]
        public string Author { get; set; }

        [Required]
        [Display(Name = "Data recenzji")]
        public DateTime ReviewDate { get; set; }

        [Required, Range(1, 6)]
        [Display(Name = "Ocena")]
        public int Rating { get; set; }

        [Required]
        [Display(Name = "Komentarz")]
        public string Content { get; set; }
    }
}

