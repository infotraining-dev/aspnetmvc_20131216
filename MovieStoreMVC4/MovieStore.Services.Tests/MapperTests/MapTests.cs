﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MovieStore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzWare.NBuilder;
using MovieStore.Services.ViewModels;
using AutoMapper;

namespace MovieStore.Services.Tests.MapperTests
{
    [TestClass]
    public class MapTests
    {
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            MovieStore.Services.Mapping.BootstrapMapping.Init();
        }

        [TestMethod]
        public void ShouldMapFromReviewToReviewViewModel()
        {
            Review review = Mother.CreateReview();

            ReviewViewModel result = Mapper.Map<ReviewViewModel>(review);

            Assert.AreEqual(review.ReviewID, result.ReviewId);
            Assert.AreEqual(review.Movie.Title, result.MovieTitle);
            Assert.AreEqual(review.Author, result.Author);
            Assert.AreEqual("2013-01-30", result.ReviewDate);
        }
    }

    static class Mother
    {
        public static Review CreateReview()
        {
            var review = Builder<Review>.CreateNew().With(r => r.ReviewDate = new DateTime(2013, 1, 30)).Build();
            review.Movie = Builder<Movie>.CreateNew().Build();

            return review ;
        }
    }
}
