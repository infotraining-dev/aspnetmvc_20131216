﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MovieStore.BDDFramework;
using MovieStore.Model;
using MovieStore.Model.Repositories;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.Services.Tests
{
    public class WithMoviesService : Context
    {
        protected MoviesService _service;
        protected Mock<IMoviesRepository> _moviesRespository;
        protected Mock<ICategoriesRepository> _categoriesRepository;

        protected override void EstablishContext()
        {
            _moviesRespository = new Mock<IMoviesRepository>();
            _categoriesRepository = new Mock<ICategoriesRepository>();
            _service = new MoviesService(_moviesRespository.Object, _categoriesRepository.Object);
        }
    }

    [TestClass]
    public class WhenGettingMovies : WithMoviesService
    {
        IEnumerable<Movie> _result;

        List<Movie> _expected = new List<Movie>()
            {
                new Movie { MovieID = 1, Title = "M1" },
                new Movie { MovieID = 2, Title = "M2" },
                new Movie { MovieID = 3, Title = "M3" },
            };

        protected override void EstablishContext()
        {
            base.EstablishContext();

            _moviesRespository.Setup(m => m.All).Returns(_expected.AsQueryable<Movie>());
        }

        protected override void BecauseOf()
        {
            _result = _service.GetMovies();
        }

        [TestMethod]
        public void ShouldCallAllOnMoviesRepository()
        {
            _moviesRespository.VerifyGet(m => m.All);
        }

        [TestMethod]
        public void ShouldReturnAllMovies()
        {
            CollectionAssert.AreEqual(_expected, _result.ToList<Movie>());
        }
    }

    [TestClass]
    public class WhenGettingCategories : WithMoviesService
    {
        IEnumerable<Category> _result;
        IQueryable<Category> _expected = new List<Category>
        {
            new Category { CategoryID = 1, Name = "C1" },
            new Category { CategoryID = 2, Name = "C2" },
            new Category { CategoryID = 3, Name = "C3" },
        }.AsQueryable();

        protected override void EstablishContext()
        {
            base.EstablishContext();
            _categoriesRepository.Setup(m => m.All).Returns(_expected);
        }

        protected override void BecauseOf()
        {
            _result = _service.GetCategories();
        }

        [TestMethod]
        public void ShouldCallAllOnCategoriesRepository()
        {
            _categoriesRepository.VerifyGet(m => m.All);
        }

        [TestMethod]
        public void ShouldReturnAllCategories()
        {
            Assert.AreSame(_expected, _result);
        }
    }
}
