﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieStore.Mvc4.MvcFilters
{
    public class TracingFilter : FilterAttribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            string controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            string actionName = filterContext.ActionDescriptor.ActionName;

            Trace.TraceInformation("Action: " + actionName + " finshed; Controller: " + controller);

            if (filterContext.Exception != null)
            {
                Trace.TraceError("Error: " + filterContext.Exception.Message);
            }
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            string actionName = filterContext.ActionDescriptor.ActionName;

            Trace.TraceInformation("Action: " + actionName + " started; Controller: " + controller);
        }
    }
}