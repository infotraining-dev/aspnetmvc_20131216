﻿using MovieStore.Model;
using MovieStore.Model.Repositories;
using MovieStore.Mvc4.Areas.Admin.ViewModels;
using MovieStore.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieStore.Mvc4.Areas.Admin.Controllers
{
    public class MoviesController : Controller
    {
        IMoviesRepository _moviesRepository;
        ICategoriesRepository _categoriesRepository;

        public MoviesController(IMoviesRepository moviesRepository, ICategoriesRepository categoriesRepository)
        {
            _moviesRepository = moviesRepository;
            _categoriesRepository = categoriesRepository;
        }

        //
        // GET: /Admin/Movies/

        public ActionResult Index()
        {
            var movies = _moviesRepository.AllIncluding(m => m.Category).ToList();

            return View(movies);
        }

        //
        // GET: /Admin/Movies/Details/5

        public ActionResult Details(int id)
        {
            var movie = _moviesRepository.Find(id);

            if (movie == null)
                return HttpNotFound();
            
            return View(movie);
        }

        //
        // GET: /Admin/Movies/Create

        public ActionResult Create()
        {
            ViewBag.CategoryList = GetCategoryList();

            return View();
        }

        private dynamic GetCategoryList()
        {
            var categories = _categoriesRepository.All.OrderBy(c => c.Name).ToList();
            SelectList categoryList = new SelectList(categories, "CategoryId", "Name");
            return categoryList;
        }

        //
        // POST: /Admin/Movies/Create

        [HttpPost]
        public ActionResult Create(Movie model, HttpPostedFileBase coverFile, HttpPostedFileBase coverThumbnailFile)
        {
            ModelState.Clear();

            model.Cover = SavePostedFile(coverFile);

            model.CoverThumbnail = SavePostedFile(coverThumbnailFile);

            if (TryValidateModel(model))
            {
                _moviesRepository.InsertOrUpdate(model);
                _moviesRepository.Save();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryList = GetCategoryList();

            return View(model);
        }

        private string SavePostedFile(HttpPostedFileBase imageFile, string folderPath = "~/Images/Covers")
        {
            if (IsFileValid(imageFile))
            {
                var fileName = Path.GetFileName(imageFile.FileName);

                var path = Path.Combine(Server.MapPath(folderPath), fileName);

                imageFile.SaveAs(path);

                return fileName;
            }

            return "";
        }

        private bool IsFileValid(HttpPostedFileBase imageFile)
        {
            return imageFile != null && imageFile.ContentLength > 0;
        }

        //
        // GET: /Admin/Movie/Edit/5

        public ActionResult Edit(int id)
        {
            MovieViewModel viewModel = new MovieViewModel
            {
                Movie = _moviesRepository.Find(id),
                CategoryList = GetCategoryList()
            };

            return View(viewModel);
        }

        //
        // POST: /Admin/Movie/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, MovieViewModel viewModel, HttpPostedFileBase coverFile, HttpPostedFileBase coverThumbnailFile)
        {
            try
            {
                string newCoverFileName = SavePostedFile(coverFile);

                if (!string.IsNullOrEmpty(newCoverFileName))
                    viewModel.Movie.Cover = newCoverFileName;

                string newCoverThumbnailFileName = SavePostedFile(coverThumbnailFile);

                if (!string.IsNullOrEmpty(newCoverThumbnailFileName))
                    viewModel.Movie.CoverThumbnail = newCoverThumbnailFileName;

                ModelState.Clear();

                if (TryValidateModel(viewModel.Movie))
                {
                    _moviesRepository.InsertOrUpdate(viewModel.Movie);
                    _moviesRepository.Save();

                    return RedirectToAction("Index");
                }
            }
            catch
            {
                ViewBag.ErrorMessage = "Błąd!";
            }

            viewModel.CategoryList = GetCategoryList();
            return View(viewModel);
        }


        //
        // GET: /Admin/Movies/Delete/5

        public ActionResult Delete(int id)
        {
            var movie = _moviesRepository.Find(id);

            return View(movie);
        }

        //
        // POST: /Admin/Movies/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                _moviesRepository.Delete(id);
                _moviesRepository.Save();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
