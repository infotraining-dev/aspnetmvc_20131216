﻿using MovieStore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace MovieStore.Mvc4.Areas.Admin.ViewModels
{
    public class MovieViewModel
    {
        public Movie Movie { get; set; }
        public SelectList CategoryList { get; set; }
    }

}
