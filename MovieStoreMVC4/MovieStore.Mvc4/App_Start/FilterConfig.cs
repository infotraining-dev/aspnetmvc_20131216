﻿using MovieStore.Mvc4.MvcFilters;
using System.Web;
using System.Web.Mvc;

namespace MovieStore.Mvc4
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new TracingFilter());
            filters.Add(new HandleErrorAttribute());
        }
    }
}