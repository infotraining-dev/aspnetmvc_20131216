﻿using MovieStore.Model;
using MovieStore.Mvc4.Bindings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieStore.Mvc4.App_Start
{
    public static class BindingConfig
    {
        public static void RegisterBindings(System.Web.Mvc.ModelBinderDictionary modelBinderDictionary)
        {
            modelBinderDictionary.Add(typeof(Cart), new CartBinder());
        }
    }
}