﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieStore.Mvc4.Code
{
    public static class DictionaryExtensions
    {
        public static SelectList ToSelectList<T>(this IDictionary<T, string> dict)
        {
            return new SelectList(dict, "Key", "Value");
        }
    }
}