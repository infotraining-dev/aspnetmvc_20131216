﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieStore.Mvc4.Code
{
    public static class StringExtensions
    {
        public static string ToShortText(this string text)
        {
            string result = text.Substring(0, 255) + "...";

            return result;
        }
    }
}