﻿using MovieStore.Model;
using MovieStore.Model.Repositories;
using MovieStore.Services.Interfaces;
using MovieStore.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieStore.Mvc4.Controllers
{
    public class CartControllerWithoutBinding : Controller
    {
        IMoviesRepository _moviesRepository;
        ICartService _cartService;

        public CartControllerWithoutBinding(IMoviesRepository moviesRepository, ICartService cartService)
        {
            _moviesRepository = moviesRepository;
            _cartService = cartService;
        }

        //
        // GET: /Cart/

        public ActionResult Index()
        {
            Cart cart = GetCartFromSession();

            CartViewModel cartViewModel = _cartService.CreateCartViewModel(cart);

            return View(cartViewModel);
        }

        public ActionResult AddToCart(int movieId)
        {
            Movie movie = _moviesRepository.Find(movieId);

            Cart cart = GetCartFromSession();

            cart.AddItem(movie);

            CartSummary cartSummary = cart.CartSummary;

            return PartialView("CartSummary", cartSummary);
        }

        public ActionResult CartSummary()
        {
            Cart cart = GetCartFromSession();

            CartSummary cartSummary = cart.CartSummary;

            return PartialView(cartSummary);
        }

        private Cart GetCartFromSession()
        {
            Cart cart = (Cart)Session["Cart"];

            if (cart == null)
            {
                cart = new Cart();
                Session["Cart"] = cart;
            }

            return cart;
        }


        public ActionResult RemoveLine(int movieId)
        {
            Movie movie = _moviesRepository.Find(movieId);

            Cart cart = GetCartFromSession();

            cart.RemoveLine(movie);

            CartViewModel cartViewModel = _cartService.CreateCartViewModel(cart);

            return Json(
                new
                {
                    DeletedMovieId = movieId,
                    TotalQuantity = cartViewModel.TotalQuantity,
                    TotalValue = cartViewModel.TotalValue
                });
        }
    }
}
