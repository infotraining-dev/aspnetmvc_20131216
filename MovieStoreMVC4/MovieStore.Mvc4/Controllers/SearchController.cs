﻿using MovieStore.Model.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieStore.Mvc4.Controllers
{
    public class SearchController : Controller
    {
        IMoviesRepository _moviesRepository;

        public SearchController(IMoviesRepository moviesRepository)
        {
            _moviesRepository = moviesRepository;
        }

        public ActionResult QuickSearch(string term)
        {
            var movies = _moviesRepository
                               .All
                               .Where(m => m.Title.Contains(term))
                               .Select(m => new { value = m.Title }).ToList();

            return Json(movies, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MovieSearch(string q)
        {
            var movies = _moviesRepository
                            .All
                            .Where(m => m.Title.Contains(q))
                            .Select(m => new
                                            {
                                                MovieId = m.MovieID,
                                                Title = m.Title,
                                                Description = m.Description,
                                                //Category = m.Category.Name,
                                                CoverThumbnail = m.CoverThumbnail
                                            }).ToList();

            return Json(movies, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Search/
        public ActionResult Index()
        {
            return View();
        }
    }
}
