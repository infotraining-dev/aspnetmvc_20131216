﻿using MovieStore.Services.Intefaces;
using MovieStore.Services.Interfaces;
using MovieStore.Services.RequestResponse;
using MovieStore.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieStore.Mvc4.Controllers
{
    public class ReviewsController : Controller
    {
        IReviewsService _reviewsService;
        IMoviesService _moviesService;

        public ReviewsController(IReviewsService reviewsService, IMoviesService moviesService)
        {
            _reviewsService = reviewsService;
            _moviesService = moviesService;
        }

        //
        // GET: /Reviews/

        public ActionResult Index()
        {
            var response = _reviewsService.GetReviews();

            if (response.Success)
            {
                return View(response.Reviews);
            }
            else
            {
                ViewBag.ErrorMessage = response.Message;
                return View("Error");
            }
        }

        public ActionResult Add(int movieId)
        {
            var movie = _moviesService.GetMovieDetails(movieId);

            var reviewVM = _reviewsService.CreateReviewViewModel(movie);

            return View(reviewVM);
        }

        [HttpPost]
        public ActionResult Add(AddReviewViewModel reviewVM)
        {
            if (ModelState.IsValid)
            {
                AddReviewRequest request = new AddReviewRequest() { Review = reviewVM };
                var response = _reviewsService.AddReview(request);

                if (response.Success)
                    return RedirectToAction("Details", "Movies", new { id = reviewVM.MovieId });
                else
                {
                    ViewBag.ErrorMessage = response.Message;
                    return View("Error");
                }
            }

            return View(reviewVM);
        }
    }
}
