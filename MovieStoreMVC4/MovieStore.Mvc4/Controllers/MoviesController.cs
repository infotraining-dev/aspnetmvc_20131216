﻿using MovieStore.DataAccessLayer.Database;
using MovieStore.Model;
using MovieStore.Services.Intefaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace MovieStore.Mvc4.Controllers
{
    public class MoviesController : Controller
    {
        IMoviesService _moviesService;

        public MoviesController(IMoviesService moviesService)
        {
            _moviesService = moviesService;
        }

        //
        // GET: /Movies/

        //[HandleError(ExceptionType=typeof(InvalidOperationException), View="MyError")]
        public ActionResult Index()
        {
            //throw new InvalidOperationException("Test handlera błędów");

            IEnumerable<Movie> movies = _moviesService.GetMovies().ToList();

            return View(movies);
        }

        public ActionResult Paged(int page = 1)
        {
            var movies = _moviesService.GetMovies().OrderBy(m => m.Title).ToPagedList(page, 5);

            return View(movies);
        }

        [ChildActionOnly]
        public ActionResult CategoryList()
        {
            var categories = _moviesService.GetCategories().ToList();

            return PartialView(categories);
        }

        public ActionResult Details(int id)
        {
            Movie movie = _moviesService.GetMovieDetails(id);

            return View(movie);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Trace.TraceError("Błąd - " + filterContext.Exception.Message);
            
            base.OnException(filterContext);
        }
    }
}
