﻿using MovieStore.Model;
using MovieStore.Model.Repositories;
using MovieStore.Mvc4.Bindings;
using MovieStore.Mvc4.Mailers;
using MovieStore.Services.Interfaces;
using MovieStore.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieStore.Mvc4.Controllers
{
    public class CartController : Controller
    {
        IMoviesRepository _moviesRepository;
        ICartService _cartService;
        IMovieStoreMailer _mailerService;

        public CartController(IMoviesRepository moviesRepository, ICartService cartService, IMovieStoreMailer mailerService)
        {
            _moviesRepository = moviesRepository;
            _cartService = cartService;
            _mailerService = mailerService;
        }

        //
        // GET: /Cart/

        public ActionResult Index([ModelBinder(typeof(CartBinder))] Cart cart)
        {
            CartViewModel cartViewModel = _cartService.CreateCartViewModel(cart);

            return View(cartViewModel);
        }

        public ActionResult AddToCart(Cart cart, int movieId)
        {
            Movie movie = _moviesRepository.Find(movieId);

            cart.AddItem(movie);

            CartSummary cartSummary = cart.CartSummary;

            return PartialView("CartSummary", cartSummary);
        }

        public ActionResult CartSummary(Cart cart)
        {
            CartSummary cartSummary = cart.CartSummary;

            return PartialView(cartSummary);
        }

        public ActionResult RemoveLine(Cart cart, int movieId)
        {
            Movie movie = _moviesRepository.Find(movieId);

            cart.RemoveLine(movie);

            CartViewModel cartViewModel = _cartService.CreateCartViewModel(cart);

            return Json(
                new
                {
                    DeletedMovieId = movieId,
                    TotalQuantity = cartViewModel.TotalQuantity,
                    TotalValue = cartViewModel.TotalValue
                });
        }

        public ActionResult Checkout(Cart cart)
        {
            CartViewModel cartVM = _cartService.CreateCartViewModel(cart);

            return View(cartVM);
        }

        [HttpPost]
        public ActionResult Checkout(Cart cart, string email)
        {
            var cartVM = _cartService.CreateCartViewModel(cart);

            _mailerService.Checkout(cartVM, email).Send();

            return RedirectToAction("Index", "Movies");
        }
    }
}
