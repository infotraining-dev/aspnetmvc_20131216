using MovieStore.Services.ViewModels;
using Mvc.Mailer;

namespace MovieStore.Mvc4.Mailers
{ 
    public class MovieStoreMailer : MailerBase, IMovieStoreMailer 	
	{
		public MovieStoreMailer()
		{
			MasterName="_Layout";
		}
		
		public virtual MvcMailMessage Checkout(CartViewModel cart, string email)
		{
            ViewData.Model = cart;

            return Populate(x =>
			{
				x.Subject = "Checkout";
				x.ViewName = "Checkout";
				x.To.Add(email);
			});
		}
 	}
}