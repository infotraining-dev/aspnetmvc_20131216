using MovieStore.Services.ViewModels;
using Mvc.Mailer;

namespace MovieStore.Mvc4.Mailers
{ 
    public interface IMovieStoreMailer
    {
			MvcMailMessage Checkout(CartViewModel cartVM, string email);
	}
}