﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MovieStore.Model;

namespace MovieStore.DataAccessLayer.Database
{
    public class MovieStoreContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Category> Categories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>()
                .HasRequired(m => m.Category)
                .WithMany(c => c.Movies)
                .HasForeignKey(m => m.CategoryID);

            modelBuilder.Entity<Review>()
                .HasRequired(r => r.Movie)
                .WithMany(m => m.Reviews)
                .HasForeignKey(r => r.MovieID);
        }
    }
}
