﻿using MovieStore.Model;
using MovieStore.Model.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.DataAccessLayer.Database.Repositories
{
    public class MoviesRepository : GenericRepository<Movie, MovieStoreContext>, IMoviesRepository
    {
        public MoviesRepository()
        {
        }

        public MoviesRepository(string databaseName)
            : base(databaseName)
        { 
        }

        protected override int GetKey(Movie entity)
        {
            return entity.MovieID;
        }
    }
}
