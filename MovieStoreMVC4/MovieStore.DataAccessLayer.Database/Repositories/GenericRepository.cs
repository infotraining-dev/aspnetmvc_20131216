﻿using MovieStore.Model.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.DataAccessLayer.Database.Repositories
{
    public abstract class GenericRepository<T, TContext> : IEntityRepository<T>
        where T : class
        where TContext : DbContext
    {
        private TContext _context;
        readonly DbSet<T> _dbSet;

        public GenericRepository()
        {
            _context = Activator.CreateInstance<TContext>();
            _dbSet = _context.Set<T>();
        }

        public GenericRepository(string databaseName)
        {
            _context = (TContext)Activator.CreateInstance(
                typeof(TContext), new object[] { databaseName });
            _dbSet = _context.Set<T>();
        }

        protected abstract int GetKey(T entity);

        public IQueryable<T> All
        {
            get { return _dbSet; }
        }

        public IQueryable<T> AllIncluding(params System.Linq.Expressions.Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _dbSet;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        public T Find(int id)
        {
            return _dbSet.Find(id);
        }

        public void InsertOrUpdate(T entity)
        {
            if (GetKey(entity) == default(int))
            {
                _context.Entry(entity).State = EntityState.Added;
            }
            else
                _context.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var entity = _dbSet.Find(id);
            _dbSet.Remove(entity);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
