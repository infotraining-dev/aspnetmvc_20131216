﻿using MovieStore.Model;
using MovieStore.Model.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.DataAccessLayer.Database.Repositories
{
    public class ReviewsRepository : GenericRepository<Review, MovieStoreContext>, IReviewsRepository
    {
        public ReviewsRepository()
        {
        }

        public ReviewsRepository(string databaseName)
            : base(databaseName)
        { 
        }

        protected override int GetKey(Review entity)
        {
            return entity.ReviewID;
        }
    }
}
