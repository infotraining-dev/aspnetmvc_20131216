﻿using MovieStore.Model;
using MovieStore.Model.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.DataAccessLayer.Database.Repositories
{
    public class CategoriesRepository : GenericRepository<Category, MovieStoreContext>, ICategoriesRepository
    {
        public CategoriesRepository()
        {
        }

        public CategoriesRepository(string databaseName)
            : base(databaseName)
        { 
        }

        protected override int GetKey(Category entity)
        {
            return entity.CategoryID;
        }
    }
}
