﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MovieStore.BDDFramework
{
    [TestClass]
    public class Context
    {
        [TestInitialize]
        public void Setup()
        {
            EstablishContext();
            BecauseOf();
        }

        virtual protected void EstablishContext()
        {
        }

        virtual protected void BecauseOf()
        {
        }

        [TestCleanup]
        public void Teardown()
        {
            Cleanup();
        }

        virtual protected void Cleanup()
        {
        }
    }
}
