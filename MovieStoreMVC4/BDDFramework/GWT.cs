﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MovieStore.BDDFramework
{
    [TestClass]
    public class GWT
    {
        [TestInitialize]
        public void Setup()
        {
            Given();
            When();
        }

        virtual protected void Given()
        {
        }

        virtual protected void When()
        {
        }

        [TestCleanup]
        public void Teardown()
        {
            Cleanup();
        }

        virtual protected void Cleanup()
        {
        }
    }
}
