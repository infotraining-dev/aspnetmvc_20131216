﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ValidationDemo.Models;

namespace ValidationDemo.Validators
{
    public class PersonValidator : AbstractValidator<Person>
    {
        public PersonValidator()
        {
            RuleSet("Names", () =>
                {
                    RuleFor(p => p.FirstName).NotEmpty().WithMessage("Imię jest wymagane");
                    RuleFor(p => p.LastName).NotEmpty().Length(2, 50);
                });

            RuleSet("Other", () =>
                {
                    RuleFor(p => p.PostalCode).NotEmpty().Matches(@"\d\d-\d\d\d").NotEqual("00-000");
                    RuleFor(p => p.Email).EmailAddress();
                    RuleFor(p => p.IsMember).Must(b => b);
                    RuleFor(p => p.MembershipStared).GreaterThan(new DateTime(2010, 1, 1)).When(p => p.IsMember);
                });
        }
    }
}