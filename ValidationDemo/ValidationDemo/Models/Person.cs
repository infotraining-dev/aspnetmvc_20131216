﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FluentValidation.Validators;
using ValidationDemo.Validators;

namespace ValidationDemo.Models
{
    
    [FluentValidation.Attributes.Validator(typeof(PersonValidator))]
    public class Person 
    {
        [Display(Name = "ID klienta")]
        public int Id { get; set; }

        [Display(Name = "Imię")]
        public string FirstName { get; set; }

        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }

        [Display(Name = "Kod pocztowy")]
        public string PostalCode { get; set; }

        public string Email { get; set; }

        [Display(Name = "Czy jesteś członkiem klubu?")]
        public bool IsMember { get; set; }

        [Display(Name = "Od kiedy")]
        public DateTime MembershipStared { get; set; }
    }
}