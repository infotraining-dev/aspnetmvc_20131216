﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ValidationDemo.Views.Home;
using ValidationDemo.Validators;

namespace ValidationDemo.Models
{
    public class Client : IValidatableObject
    {
        [Display(Name="ID klienta")]
        public int Id { get; set; }

        [Display(Name="Imię")]
        [Required]
        public string FirstName { get; set; }

        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }

        [Display(Name = "Kod pocztowy")]
        [RegularExpression(@"\d\d-\d\d\d", ErrorMessageResourceName = "PostalCodeErrorMsg", ErrorMessageResourceType = typeof(Resources))]
        public string PostalCode { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        [Display(Name = "Czy jesteś członkiem klubu?")]
        [TrueRequried(ErrorMessage = "Członkostwo jest wymagane")]
        public bool IsMember { get; set; }

        [Display(Name = "Od kiedy")]    
        public DateTime MembershipStared { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> results = new List<ValidationResult>();

            if (MembershipStared < new DateTime(2010, 1, 1))
            {
                results.Add(new ValidationResult("Nieprawidłowa data", new[] { "MembershipStared" }));
            }

            return results;
        }
    }
}