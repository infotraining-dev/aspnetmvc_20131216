﻿using FluentValidation.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ValidationDemo.Models;

namespace ValidationDemo.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Client client)
        {   
            if (ModelState.IsValid)
            {
                return View("Details", client);
            }

            return View(client);
        }

        public ActionResult CreatePerson()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreatePerson([CustomizeValidator(RuleSet="Names")] Person client)
        {
            if (ModelState.IsValid)
            {
                return View("PersonDetails", client);
            }

            return View(client);
        }

    }
}
