namespace SecurityDemo.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Web.Security;
    using WebMatrix.WebData;

    internal sealed class Configuration : DbMigrationsConfiguration<SecurityDemo.Models.UsersContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SecurityDemo.Models.UsersContext context)
        {
            WebSecurity.InitializeDatabaseConnection("DefaultConnection", "UserProfile", "UserId", "UserName", true);

            if (!Roles.RoleExists("administrator"))
                Roles.CreateRole("administrator");

            if (!WebSecurity.UserExists("janek"))
                WebSecurity.CreateUserAndAccount("janek", "passwd", new { Address = "Kalwaryjska 35", City = "Krak�w" });

            if (!Roles.GetUsersInRole("administrator").Contains("janek"))
                Roles.AddUserToRole("janek", "administrator");
        }
    }
}
