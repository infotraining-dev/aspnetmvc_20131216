namespace SecurityDemo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddressCity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfile", "Address", c => c.String());
            AddColumn("dbo.UserProfile", "City", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserProfile", "City");
            DropColumn("dbo.UserProfile", "Address");
        }
    }
}
